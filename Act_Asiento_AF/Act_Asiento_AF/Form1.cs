﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Act_Asiento_AF
{
    public partial class Form1 : Form
    {
        public static class Compania
        {
            public static string CodCompania;
        }
        public static class connetionString
        {
            public static string connetionStringC;
        }
        public static class Asiento
        {
            public static string CodAsiento;
        }
        public static class BaseDatos
        {
            public static string CodBase;
        }
        public Form1(string Ccia, string Base)
        {
            Compania.CodCompania = Ccia;
            BaseDatos.CodBase = Base;
            InitializeComponent();
            connetionString.connetionStringC = "Data Source=192.168.7.2;Initial Catalog="+BaseDatos.CodBase.ToString()+";Persist Security Info=True;User ID=sa;Password=jda";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SqlConnection cnn;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            string sql2 = null;


            cnn = new SqlConnection(connetionString.connetionStringC);
            sql = "SELECT HD.ASIENTO FROM " + Compania.CodCompania.ToString() + ".GLOBALES_AF ga INNER JOIN "+Compania.CodCompania.ToString()+".HIST_DEPRECIACION hd ON GA.ULT_DEPRECIACION = HD.HIST_DEPRECIACION ;";
            try
            {

                cnn.Open();
                adapter.InsertCommand = new SqlCommand(sql, cnn);
                Asiento.CodAsiento = adapter.InsertCommand.ExecuteScalar().ToString();
                sql2 = "SELECT COUNT(*) FROM "+Compania.CodCompania.ToString()+".ASIENTO_MAYORIZADO am WHERE am.ASIENTO='"+Asiento.CodAsiento.ToString()+"';";
                adapter.SelectCommand= new SqlCommand(sql2, cnn);
                string EvaAsiento = adapter.SelectCommand.ExecuteScalar().ToString();
                if (EvaAsiento == "1")
                {
                    MessageBox.Show("Partida se encuentra en el mayor, proceso no se puede llevar a cabo");
                    this.Close();
                }
                else
                {
                    label1.Text = "Este proceso actulizara la partida con las cuentas de los comodato para el Asiento:"+ Asiento.CodAsiento.ToString() ;
                    cnn.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

        }

        private void BtnNo_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BtnSI_Click(object sender, EventArgs e)
        {
            SqlConnection cnn;
            SqlDataAdapter adapter = new SqlDataAdapter();
            string sql = null;
            string sqlv = null;

            cnn = new SqlConnection(connetionString.connetionStringC);

           
            string sql2 = null;
            string data = null;
            int i = 0;
            int j = 0;

            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Add(misValue);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

           
            
            cnn.Open();
            sql = "SELECT  * FROM "+Compania.CodCompania.ToString()+".DIARIO WHERE asiento='"+Asiento.CodAsiento.ToString()+"'";
            SqlDataAdapter dscmd = new SqlDataAdapter(sql, cnn);
            DataSet ds = new DataSet();
            dscmd.Fill(ds);

            for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
            {
                for (j = 0; j <= ds.Tables[0].Columns.Count - 1; j++)
                {
                    data = ds.Tables[0].Rows[i].ItemArray[j].ToString();
                    xlWorkSheet.Cells[i + 1, j + 1] = data;
                }
            }

            xlWorkBook.SaveAs(@"C:\SoftlandERP\Partida"+ Asiento.CodAsiento.ToString() + ".xls");
            xlWorkBook.Close(true, misValue, misValue);
            xlApp.Quit();

            releaseObject(xlWorkSheet);
            releaseObject(xlWorkBook);
            releaseObject(xlApp);
            cnn.Close();

            sqlv = "EXECUTE dbo.Act_CTA_comodato_Valida @pais = '" + Compania.CodCompania.ToString() + "'; ";
            cnn.Open();
            adapter.SelectCommand = new SqlCommand(sqlv, cnn);
            var cont = adapter.SelectCommand.ExecuteReader();
            string mensaje = "Favor asociar los siguientes par de cuentas centros: (MODULO CG->CUENTAS-Centro de costo ) ";


            if(cont.HasRows)
            {
                while(cont.Read())
                {
                    mensaje = mensaje.ToString() + " " + cont.GetString(0) + " " + cont.GetString(1) + " " + cont.GetString(2);
                }
                MessageBox.Show(mensaje);
                this.Close();
            }
            cnn.Close();





            sql = "EXECUTE dbo.Act_CTA_comodato @pais = '"+Compania.CodCompania.ToString()+"'; ";
            try
            {

                cnn.Open();
                adapter.InsertCommand = new SqlCommand(sql, cnn);
                adapter.InsertCommand.ExecuteNonQuery();
                MessageBox.Show("Se actulizo la partida");
                
                cnn.Close();
                this.Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
        private void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                MessageBox.Show("Exception Occured while releasing object " + ex.ToString());
            }
            finally
            {
                GC.Collect();
            }
        }

        private void Label1_Click(object sender, EventArgs e)
        {

        }
    }
}
